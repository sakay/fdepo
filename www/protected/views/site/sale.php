<?php

if (!isset(Yii::app()->session['store']) && Yii::app()->session['role'] == 'admin') {
    echo "Admin olarak bir mağazadan satış yapamazsınız.<br/>";
    echo '<a href="/site/index">Ana Sayfa</a><br/>';
    echo '<a href="/site/logout">logout</a><br/>';
    exit;
}

if (strlen($productQntErrString) > 0) {
    ?><script type="text/javascript">toastr.error('Mağaza Stoğundan Olan Ürün sayısından fazlası satılmak istendi. Tüm satışlar iptal edildi. Stok Bilgisi: <br> <?php echo $productQntErrString;?>');</script><?php
} else if (isset($success) && $success == true) {?>
    <script type="text/javascript">toastr.success('Satış Kaydedildi  ');</script>
    <?php
} else if ($success == -1) {
    ?><script type="text/javascript">toastr.error('Satış Kaydedilmedi. Hata oluştu');</script><?php
}

$form=$this->beginWidget('CActiveForm', array(
    'id'=>'sale-form',
    'enableClientValidation'=>true,
    'method'=>'post',
    'action' => '/site/sale',
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
    'focus'=>array($model,'product_code'),
    'htmlOptions'=>array(
        'onSubmit'=>'return false',
    ),
));
?>

<div class="row-fluid">
<div class="span5">
    <!-- Your first column here -->
    <div class="container">
                <div id="dcode" class="col-md-2 col-md-offset-2">
                    <?php echo $form->labelEx($model,'Ürün Kodu'); ?>
                    <?php echo $form->textField($model,'product_code',array('name'=>'product_code','value'=>'', 'style'=>'height:50px;')); ?>
                    <span class="icon-large icon-barcode"></span>

                </div>
                <div class="col-md-2 col-md-offset-2 margin-top-10">
                    <button type="button" name="paymentCash" id="paymentCash" onclick="setPayment(1)" class="btn btn-success">Nakit</button>
                    <button type="button" name="paymentCC" id="paymentCC" onclick="setPayment(2)" class="btn btn-success">Kredi Kartı</button>
                    <input type="hidden" name="paymentMethod" id="paymentMethod" value=""/>
                </div>
                <div class="col-md-2 col-md-offset-2 margin-top-10">
                    <?php foreach ($storePersonel as $valObj) { ?>
                        <button type="button" name="staffName_<?php echo $valObj->id?>" id="staffName_<?php echo $valObj->id?>" onclick="setPersonel(<?php echo $valObj->id?>)" class="btn btn-danger"><?php echo $valObj->person_name?></button>
                    <?php }?>
                    <input type="hidden" name="personelID" id="personelID" value=""/>
                </div>
                <div class="col-md-1 col-md-offset-1 margin-top-10">
                    <table  style="width: 250px" class="table table-hover" id="saleTable">
                        <tbody id="saleBody2">
                        <tr>
                            <td colspan=""><h3>Toplam <strong id="totalPrice">0 TL</strong></h3></td>
                        </tr>
                        </td>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-2 col-md-offset-2 margin-top-30">
                    <button type="button" class="btn btn-danger active btn-large" onclick="window.location = location.href">Vazgeç</button>
                    <button onclick="if (submitSale())document.sale-form.submit();" type="button" class="btn btn-success btn-large">Kaydet / Bitir</button></td>
                </div>


    </div>
</div>
<div class="span1">
    <!-- Your second column here -->
<div class="offset1">
    <div class="span9">
        <div style="width: 500px">
            <table class="table table-hover" id="saleTable">
                <thead>
                <tr>
                    <th>Ürün Kodu</th>
                    <th>Kategori</th>
                    <th>Adet</th>
                    <th class="text-center">Fiyat</th>
                    <th> </th>
                </tr>
                </thead>
                <tbody id="saleBody">

                </td>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
</div>
<?php $this->endWidget();

?>
<script>
var total = 0;
var itemArr = [];
var itemIndex = 0;

$("#product_code").keypress(function(e) {

        if(e.which == 13) {
            if ($('#product_code').val() == '') {
                return false;
            }
            $.ajax({
                url:"AjaxOutCheck",
                data:'product_code='+$('#product_code').val(),
                success:function(result) {
                    $('#product_code').val("");
                    var obj = jQuery.parseJSON(result);
                    if (obj.err == -1) {
                        toastr.error('Ürün bulunamadı');
                        return false;
                    }
                    if (obj.err == -2) {
                        toastr.error('Ürün mağaza stoğunda yok');
                        return false;
                    }
                    if (obj.price == 0) {
                        toastr.warning('Ürün 0 TL');
                    }

                    var table = document.getElementById("saleBody");
                    var row = table.insertRow(0);
                    total = (total + parseFloat(obj.price));
                    itemArr.push({
                        index: itemIndex,
                        product_code: obj.product_code,
                        price: parseFloat(obj.price)
                    });

                    document.getElementById("totalPrice").innerHTML = total.toFixed(2) + ' TL';
                    row.innerHTML = '<tr><td class="col-sm-8 col-md-6"><div class="media">'
                            +'<div class="media-body"><h4 class="media-heading">' +
                            '<input type="text" style="width:90px" name="pcode['+itemIndex+']" readonly="readonly" value="'+obj.product_code+'"></h4></div></div></td>' +
                            '<td class="media-heading"><strong>'+obj.category_name+'</strong></td>' +
                            '<td class="media-heading"><strong>1</strong></td>' +
                            '<td class="col-sm-1"><strong><input type="text" readonly="readonly" style="width:50px" name="price['+itemIndex+']" value ="'+obj.price+'"></strong></td>' +
                            '<td class=""><button type="button" onclick="substractPrice('+itemIndex+');this.parentNode.parentNode.innerHTML = \'\'" class="btn btn-danger"><span id="remove_'+itemIndex+'" class="glyphicon glyphicon-remove"></span> Sil</button></td></tr>';
                    itemIndex++;
                }
            });
        }
    });

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-top-full-width",
        "onclick": null,
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }


    function substractPrice(id) {
        for (var i = 0; i < itemArr.length; i++) {
            if (itemArr[i].index == id) {
                total = total - itemArr[i].price;
                document.getElementById("totalPrice").innerHTML = total.toFixed(2) + " TL";
            }
        }
    }

    function setPersonel(person_id) {
        if ($('#personelID').val()) {
            $('#staffName_'+$('#personelID').val()).attr('class', 'btn btn-danger');
        }
        $('#staffName_'+person_id).attr('class', 'btn active btn-danger');
        $('#personelID').val(person_id);
    }
    function setPayment(type) {
        if (type == 1) {
            $('#paymentCash').attr('class', 'btn active btn-success');
            $('#paymentCC').attr('class', 'btn btn-success');
        } else if (type == 2) {
            $('#paymentCash').attr('class', 'btn btn-success');
            $('#paymentCC').attr('class', 'btn active btn-success');
        }
        $('#paymentMethod').val(type);
    }
    function submitSale() {
        if (!$('#paymentMethod').val()) {
            toastr.error('Ödeme şeklini seçiniz');
            return false;
        }
        if (!$('#personelID').val()) {
            toastr.error('Satışı yapan personeli seçiniz');
            return false;
        }
        return true;
    }
</script>




