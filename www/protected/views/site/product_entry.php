<?php


$form=$this->beginWidget('CActiveForm', array(
    'id'=>'entry-form',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
    'htmlOptions'=>array(
        'class'=>'span7',
        'enctype' => 'multipart/form-data',
    ),
));
?>

<div class="controls-row" xmlns="http://www.w3.org/1999/html">
    <div class="span6">
        <form action="" method="post" class="form-horizontal" id="product_form" accept-charset="utf-8">
            <?php

            if ($success_code) {
                if (isset($edit)) {
                    ?><script>toastr.success('Ürün düzenlendi.');</script><?php
                } else {
                    ?> <script>toastr.success('Ürün kayıt edildi.');</script>
                <?php }?>
                <p><span class="label label-success"><?php echo $success_code?> - <?php if(isset($edit)) { echo 'Düzenlendi';} else {echo 'Kayıt edildi.';}?></span></p>
            <?php } ?>
                    <div class="control-group">
                        <?php echo $form->labelEx($model,'Kategori'); ?>
                        <?php echo $form->dropDownList($model, 'category_id', $allCategories ,array('empty' => '(Seçiniz)',
                            'options' => isset($productObj->category_id)?array($productObj->category_id=>array('selected'=>true)):'')); ?>
                        <?php echo $form->error($model,'category_id'); ?>

                    </div>
                <?php if (!isset($edit)) { ?>
                    <div class="control-group">
                        <?php echo $form->labelEx($storeModel,'Mağaza'); ?>
                        <?php echo $form->dropDownList($storeModel, 'id', $storeArr ,array('empty' => '(Seçiniz)',
                            'options' => isset($productObj->category_id)?array($productObj->category_id=>array('selected'=>true)):'')); ?>
                        <?php echo $form->error($storeModel,'id'); ?>

                    </div>
                <?php }?>

                <div id="dcode" class="control-group">
                    <?php echo $form->labelEx($model,'Ürün Kodu'); ?>
                    <?php echo $form->textField($model,'code',array('style'=>'height:50px;','value'=>isset($productObj->code)? $productObj->code :'',
                    'ajax' => array(
                        'type'=>'POST', //request type
                        'url'=>CController::createUrl('site/AjaxCodeChecker'), //url to call.
                        'success'=>'function(data){
                            if (data == 1) {
                                toastr.error(\'Bu urun kodu kullanimda\');
                            }
                         }'
                    ))); ?>
                    <span class="icon-large icon-barcode"></span>
                    <?php echo $form->error($model,'code'); ?>

                </div>

                <div class="control-group">
                    <?php echo $form->labelEx($model,'Başlık'); ?>
                    <?php echo $form->textField($model,'title',array('value'=>isset($productObj->title)? $productObj->title :'')); ?>
                    <?php echo $form->error($model,'title'); ?>
                </div>

                <div class="control-group">
                    <?php echo $form->labelEx($model,'Açıklama'); ?>
                    <?php echo $form->textArea($model,'description',array('rows'=>5,'value'=>isset($productObj->description)? $productObj->description :'')); ?>
                    <?php echo $form->error($model,'description'); ?>
                </div>

                <div class="control-group">
                    <?php echo $form->labelEx($model,'Alış Fiyatı'); ?>
                    <?php echo $form->textField($model,'buy_price',array('value'=>isset($productObj->buy_price)? $productObj->buy_price :'')); ?>
                    <?php echo $form->error($model,'buy_price'); ?>
                </div>

                <div class="control-group">
                    <?php echo $form->labelEx($model,'Satış Fiyatı 1'); ?>
                    <?php echo $form->textField($model,'sale_price1',array('value'=>isset($productObj->sale_price1)? $productObj->sale_price1 :'')); ?>
                    <?php echo $form->error($model,'sale_price1'); ?>
                </div>

                <div class="control-group">
                    <?php echo $form->labelEx($model,'Satış Fiyatı 2'); ?>
                    <?php echo $form->textField($model,'sale_price2',array('value'=>isset($productObj->sale_price2)? $productObj->sale_price2 :'')); ?>
                    <?php echo $form->error($model,'sale_price2'); ?>
                </div>
                <?php if (!isset($edit)) { ?>
                <div class="control-group">
                    <?php echo $form->labelEx($model,'Adet'); ?>
                    <?php echo $form->textField($model,'quantity',array('value'=>isset($productObj->quantity)? $productObj->quantity :'1')); ?>
                    <?php echo $form->error($model,'quantity'); ?>
                </div>
                <?php }?>
                <div class="control-group">
                    <?php echo $form->labelEx($model,'Beden'); ?>
                    <?php echo $form->textField($model,'size',array('value'=>isset($productObj->size)? $productObj->size :'')); ?>
                    <?php echo $form->error($model,'size'); ?>
                </div>

                <div class="control-group">
                    <?php echo $form->labelEx($model,'Renk'); ?>
                    <?php echo $form->textField($model,'color',array('value'=>isset($productObj->color)? $productObj->color :'')); ?>
                    <?php echo $form->error($model,'color'); ?>
                </div>
                <div class="control-group">
                    <?php if (isset($photoArr['images']) && isset($edit)) {
                        foreach ($photoArr['images'] as $id => $img_url) {
                    ?>
                    <div style="margin:10px;float: left">
                    <img id="image_<?php echo $id;?>" class="media-object dp img-circle" src="<?php echo Yii::app()->request->baseUrl."/images/uploads/thumbs/".$img_url;?>" style="width: 100px;height:100px;">
                    <span style="display: block; text-align: center"><?php
                        echo CHtml::ajaxLink('Sil',Yii::app()->createUrl('site/DeletePicture',array('id'=>$id,'image'=>$img_url)),
                                array(
                                    'type'=>'POST',
                                    'data'=> 'js:$("#dataContainer").serialize()',
                                    'success'=>'js:function(res){
                                        if (res == -1) {
                                            toastr.error(\'Hata Oluştu.\');
                                        } else {
                                            $(\'#image_text_\'+res).hide();
                                            $(\'#image_\'+res).hide();
                                            toastr.success(\'Resim Silindi.\');
                                        }
                                     }'
                                ),array('id'=>'image_text_'.$id,));
                        ?>

                    </span>
                    </div>
                    <?php }}?>
                </div>
            <div class="control-group" style="clear: both">
                <input type="hidden" name="MAX_FILE_SIZE" value="5900000" />
                <input type="file" name="dosya[]" id="dosya[]" multiple="multiple" />
            </div>

            <div class="form-actions">
                <button type="submit" class="btn btn-large btn-primary"><?php echo isset($edit) ?'Ürün Düzenle':'Ürünü Kaydet';?></button>
            </div>
        </form>
    </div> <!-- .span6 -->
</div>

<?php if (isset($lastRow) && !empty($lastRow)) {
    $firstPhoto = '';
        if(isset($photoArr['images'])) {
            $firstPhoto = array_values($photoArr['images']);
            $firstPhoto = $firstPhoto[0];
        }
    ?>

    <a href="#" class="close"></a>
    <div class="row-fluid">
        <ul class="thumbnails">
            <li class="span3">
                <div class="thumbnail" style="padding: 0">
                    <div class="caption">
                        <p>Son Girilen Ürün<p>
                    </div>
                    <div style="padding:4px">
                        <img width="250" height="150" src="<?php echo !empty($firstPhoto)? Yii::app()->request->baseUrl."/images/uploads/thumbs/".$firstPhoto:'';?>">
                    </div>
                    <div class="caption">
                        <h2><?php echo $lastRow->code;?></h2>
                        <p><?php echo $lastRow->title;?></p>
                        <?php if (isset($lastRow->buy_price)) {?><p><i class="icon icon-tag"></i> <?php echo $lastRow->buy_price;?> TL</p><?php } ?>
                        <p><?php echo CHtml::link('Düzenle',array('site/edit','product_id'=>$lastRow->id)); ?></p>
                    </div>
                </div>
            </li>
        </ul>

    </div>

<?php }?>
<?php $this->endWidget();?>
<script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-top-full-width",
        "onclick": null,
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
</script>