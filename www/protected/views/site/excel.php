<?php
$form=$this->beginWidget('CActiveForm', array(
    'id'=>'ff',
    'method'=>'get',
    'action' => '/site/excel'
));
?>
<div class="row">
    <div class="span-8">
        <?php echo $form->labelEx($model,'Kategori'); ?>
        <?php
        $selections = array(
            'all' => 'Tüm Ürünler',
            '1' => 'Kolye',
            '2' => 'Küpe',
            '3' => 'Bileklik',
            '4' => 'Yüzük',
            '5' => 'Çanta',
            '6' => 'Cüzdan',
            '7' => 'Hediyelik',
            '8' => 'Şal',

        );
        ?>
            <?php echo $form->dropDownList($model, 'category_id', $selections,array('name'=>'category_id','all' => 'Tüm Ürünler',
                'options' => isset($params['category_id'])?array($params['category_id']=>array('selected'=>true)):''));?>
    </div>
    <div class="span-8 last">
        <?php echo $form->labelEx($model,'Ürün Kodu'); ?>
        <?php echo $form->textField($model,'title',array('name'=>'code','value'=>isset($params['code'])? $params['code'] :'')); ?>
        &nbsp;&nbsp;&nbsp;<span style="vertical-align: top"> <button type="submit" class="btn btn-primary btn-small">Filtrele</button></span>
    </div>

</div>

<?php $this->endWidget();

$this->widget('ext.htmlTableUi.htmlTableUi',array(
    'ajaxUrl'=>'site/hadleExcelAjax',
    'arProvider'=>'',
    'collapsed'=>false,
    'columns'=>$columnsArray,
    'cssFile'=>'',
    'editable'=>true,
    'enableSort'=>true,
    'exportUrl'=>'site/exportTable',
    'extra'=>'İşlemler:',
    'footer'=> 'Sayfa: '. $currentPage.'/'.$totalPageCount . '<br/> Toplam Ürün: ' . $params['totalFound'],
    'formTitle'=>'Düzenle',
    'rows'=>$rowsArray,
    'sortColumn'=>1,
    'sortOrder'=>'desc',
    'title'=>'Femaly Accessories',
    'subtitle'=>'Ürün Listesi',
));


unset($_GET['sf']);
$reqUri = http_build_query($_GET);
if (strlen($reqUri) > 1) {
    $reqUri = '?'.$reqUri;
}

$reqUri .= $reqUri? '&':'?';

?>

<div class="text-center">
    <ul class="pager">
        <?php
        for($i = 1; $i <= $totalPageCount; $i++ ) {
            if ($i == 1) {
                if ($currentPage == 1) {
                    echo '<li class="disabled"><span>&laquo;</span></li>';
                } else {
                    echo '<li><a href="'.$reqUri.'sf='.($currentPage - 1).'">&laquo;</a></li>';
                }
            }

            if ($i == $currentPage) {
                echo '<li class="active"><span>'.$i.'</span></li>';
            } else {
                echo '<li><a href="'.$reqUri.'sf='.$i.'">'.$i.'</a></li>';
            }

            if ($i == $totalPageCount) {
                if ($currentPage == $i) {
                    echo '<li class="disabled"><span>&raquo;</span></li>';
            } else {
                    echo '<li><a href="'.$reqUri.'sf='.($currentPage + 1).'">&raquo;</a></li>';
                }
            }
        }
        ?>
    </ul>
</div>