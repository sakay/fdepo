

<?php
if ($login) {
    ?>
<div style="margin:auto;width:80%">
    <?php if (Yii::app()->session['role'] == "admin") {?>
    <div class="span4">
        <a href="<?php echo Yii::app()->request->baseUrl;?>/site/entry" class="btn btn-primary btn-large btn-block">
            <i class="icon-arrow-down icon-white"></i>
            <span><strong>Ürün Girişi</strong></span>
        </a>
    </div>
    <div class="span4">
        <a href="<?php echo Yii::app()->request->baseUrl;?>/site/out" class="btn btn-info btn-large btn-block">
            <i class="icon-arrow-up icon-white"></i>
            <span><strong>Ürün Transfer</strong></span>
        </a>
    </div>
    <?php }?>
    <div class="span4">
        <a href="<?php echo Yii::app()->request->baseUrl;?>/site/sale" class="btn btn-success btn-large btn-block">
            <i class="icon-shopping-cart icon-white"></i>
            <span><strong>Satış</strong></span>
        </a>
    </div>
    <div class="span4">
        <a href="<?php echo Yii::app()->request->baseUrl;?>/report/index" class="btn btn-warning btn-large btn-block">
            <i class="icon-signal icon-white"></i>
            <span><strong>Rapor</strong></span>
        </a>
    </div>
    <div class="span4">
        <a href="<?php echo Yii::app()->request->baseUrl;?>/site/cancel" class="btn btn-danger btn-large btn-block">
            <i class="icon-remove icon-white"></i>
            <span><strong>Satış İptal</strong></span>
        </a>
    </div>
</div>
<?php } else {


$form=$this->beginWidget('CActiveForm', array(
    'id'=>'login-form',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>

<form class="form-horizontal" action='' method="POST">
    <fieldset>
        <div id="legend">
            <legend class="">Login</legend>
        </div>
        <div class="control-group">
            <!-- Username -->
            <div class="controls">
                <?php echo $form->labelEx($model,'username'); ?>
                <?php echo $form->textField($model,'username'); ?>
                <?php echo $form->error($model,'username'); ?>
            </div>

            <div class="control-group">
                <?php echo $form->labelEx($model,'password'); ?>
                <?php echo $form->passwordField($model,'password'); ?>
                <?php echo $form->error($model,'password'); ?>
            </div>
        </div>

        <div class="control-group">
            <!-- Button -->
            <div class="controls">
                <?php echo CHtml::submitButton('Login',array('class'=>'btn btn-success')); ?>
            </div>
        </div>
    </fieldset>
</form>
<?php
$this->endWidget();
} // end check if user log in
    ?>