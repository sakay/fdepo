<?php


$form=$this->beginWidget('CActiveForm', array(
    'id'=>'entry-form',
    'enableClientValidation'=>true,
    'method'=>'get',
    'action' => '/site/out',
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
    'focus'=>array($model,'code'),
    'htmlOptions'=>array(
        //'onSubmit'=>'return false',
    ),
));
?>

    <div class="span4 offset4">
                <div id="dcode" class="control-group">
                    <?php echo $form->labelEx($model,'Ürün Kodu'); ?>
                    <?php echo $form->textField($model,'code',array('name'=>'code','value'=>isset($_GET['code'])? $_GET['code'] :'', 'style'=>'height:50px;')); ?>
                    <span class="icon-large icon-barcode"></span>

                </div>

        <?php

        if ($hideInputs == false && $err == false && isset($_GET['code']) && !empty($_GET['code'])) {
            foreach($allStores as $storeId => $val) { ?>
                <div id="dcode" class="control-group">
                    <?php echo '<span style="padding-left:20px">'.$val.' '.$form->textField($model,'quantity',array('name'=>'store['.$storeId.']','style'=>'height:20px;width:30px', 'value'=>isset($inventory[$storeId]['quantity'])? $inventory[$storeId]['quantity'] :0)).'</span>'; ?>
                </div>
            <?php } ?>

        <div class="form-actions">
            <button type="submit" name="submit" value="1" class="btn btn-large btn-primary">Adet Düzenle</button>
        </div>
            <?php } ?>
    </div> <!-- .span8 -->
<?php if ($err == true) { ?>
    <script>toastr.error('Ürün bulunamadı.');</script>
<?php } ?>
<?php if ($success == true) { ?>
    <script>toastr.success('Adetler güncellendi.');</script>
<?php } ?>

<?php $this->endWidget();
/*
?>
<script>

    $(document).keypress(function(e) {
        if(e.which == 13) {
            $.ajax({
                url:"AjaxOutCheck",
                data:'code='+$('#EntryForm_code').val(),
                success:function(result) {
                    if (result > 0) {
                        toastr.success($('#EntryForm_code').val() + ' kodlu üründen 1 adet çıkış yapıldı. Kalan adet: <span style="font-size: 72px">' + result + '</span>');
                    } else if (result == -1 ) {
                        toastr.error('Hata oluştu. ' + $('#EntryForm_code').val() + ' kodlu üründen çıkış yapılmadı.');
                    } else if (result == -2 ) {
                        toastr.error('Hata oluştu. ' + $('#EntryForm_code').val() + ' kodlu üründen 0 adet var.');
                    } else if (result == -3 ) {
                        toastr.error('Hata oluştu. ' + $('#EntryForm_code').val() + ' kodlu ürün bulunamadı.');
                    }
                }
            });
        }
    });

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-top-full-width",
        "onclick": null,
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
</script>
*/

