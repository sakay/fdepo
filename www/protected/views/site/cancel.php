<?php

if (!isset(Yii::app()->session['store']) && Yii::app()->session['role'] == 'admin') {
    echo "Admin olarak bir mağazadan satış iptali yapamazsınız.<br/>";
    echo '<a href="/site/index">Ana Sayfa</a><br/>';
    echo '<a href="/site/logout">logout</a><br/>';
    exit;
}
/*
if (strlen($productQntErrString) > 0) {
    ?><script type="text/javascript">toastr.error('Mağaza Stoğundan Olan Ürün sayısından fazlası satılmak istendi. Tüm satışlar iptal edildi. Stok Bilgisi: <br> <?php echo $productQntErrString;?>');</script><?php
} else if (isset($success) && $success == true) {?>
    <script type="text/javascript">toastr.success('Satış Kaydedildi  ');</script>
    <?php
} else if ($success == -1) {
    ?><script type="text/javascript">toastr.error('Satış Kaydedilmedi. Hata oluştu');</script><?php
}
*/
$form=$this->beginWidget('CActiveForm', array(
    'id'=>'entry-form',
    'enableClientValidation'=>true,
    'method'=>'get',
    'action' => '/site/cancel',
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
    'focus'=>array($model,'sale_id'),
    'htmlOptions'=>array(
        //'onSubmit'=>'return false',
    ),
));
?>

    <div class="offset4">
        <div id="dcode" class="control-group">
            <?php echo $form->labelEx($model,'Satış Id'); ?>
            <?php echo $form->textField($model,'sale_id',array('name'=>'sale_id','value'=>isset($_GET['sale_id'])? $_GET['sale_id'] :'', 'style'=>'height:50px;')); ?>
            <span class="icon-large icon-barcode"></span>
        </div>
    </div> <!-- .span8 -->
<?php if (isset($saleObj->id)) {?>
    <div class="bs-example divider">
        <table class="table">
            <thead>
            <tr>
                <th colspan="5" style="text-align: center;background-color: #bdccff;">Satış Bilgisi</th>
            </tr>
            <tr>
                <th>ID</th>
                <th>Ürün Kodu</th>
                <th>Fiyat</th>
                <th>Satış Durumu</th>
                <th>Tarih</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><?php echo $saleObj->id;?></td>
                <td><?php echo $saleObj->code;?></td>
                <td><?php echo $saleObj->price?></td>
                <td><?php echo $saleObj->status == 1?'Satış Başarılı':'İptal Edilmiş';?></td>
                <td><?php echo $saleObj->idate?></td>
            </tr>
            <tr>
                <td colspan="5" style="text-align: center">
                    <input type="hidden" name="cancel" value="<?php echo $saleObj->status == 1?'1':'-1';?>">
                    <button  type="submit" class="btn btn-<?php echo $saleObj->status == 1?'danger':'success';?> btn-large"><?php echo $saleObj->status == 1?'İPTAL ET':'AKTİF SATIŞ YAP';?></button></td>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
<?php }?>
<?php if (isset($err) && $err == 'other_store') { ?>
    <script>toastr.error('Başka Mağaza Satışı.');</script>
<?php } elseif (isset($err) && $err == 'not_found') { ?>
    <script>toastr.error('Satış ID Bulunamadı.');</script>
<?php } ?>
<?php if (isset($success) &&  $success == true && $saleObj->status == -1) { ?>
    <script>toastr.success('Satış İptal Edildi.');</script>
<?php }else if (isset($success) &&  $success == true && $saleObj->status == 1) { ?>
    <script>toastr.success('Satış Aktif Yapıldı.');</script>
<?php } ?>
<?php $this->endWidget();



