<div class="span6">
    <h5>Ürün kategori dağılımı</h5>
    <?php foreach ($categoryCountArr as $catName => $val) {
        $percent = round(($val['count']/$total)*100);
        ?>
    <strong><?php echo $catName;?></strong><span class="pull-right"><?php echo $val['count'].' ('.$percent;?>%)</span>
    <div class="progress progress-success active">
        <div class="bar" style="width: <?php echo $percent;?>%;"></div>
    </div>
    <?php } ?>
</div>