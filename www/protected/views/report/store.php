<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sakay
 * Date: 10/28/14
 * Time: 11:58 AM
 * To change this template use File | Settings | File Templates.
 */

?>
<div id="wrapper">

<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
<div class="navbar-header">
    <a class="navbar-brand" href="/">iFemaly</a>
</div>

<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a class="" href="/report/index"><i class="fa fa-bar-chart-o fa-fw"></i> Rapor Anasayfa</a>
            </li>
            <?php

            foreach ($storeObj as $val) {
                if ($val->id == $storeId) {
                    $currentStoreObj = $val;
                }
                $mActive = '';
                if ($storeId == $val->id) {
                    $mActive = 'active';
                }
                echo '<li><a class="'.$mActive.'" href="/report/store?store_id='.$val->id.'"><i class="fa fa-line-chart fa-fw"></i> '.$val->name.' </a></li>';
            }
            ?>
            <form name="dateSubmit" id="dateSubmit" method="post">
                <li><span style="padding-left: 20px;margin: 10px;" class="fa fa-calendar"> Başla: <input type="text" name="start" id="datepicker"></span></li>
                <li><span style="padding-left: 20px;margin: 10px;" class="fa fa-calendar"> Bitiş: <input type="text" name="end" id="datepicker2"></span></li>
                <input type="hidden" name="store_id" id="store_id" value="<?php echo $storeId;?>"/>
                <li><button class="btn center-block btn-danger btn-small" type="submit">Gönder</button></li>
            </form>
            <li><a href="javascript:setDates(1);"><i class="fa fa-calendar fa-fw"></i>Dün</a></li>
            <li><a href="javascript:setDates(7);"><i class="fa fa-calendar fa-fw"></i> Son 7 gün</a></li>
            <li><a href="javascript:setDates(30);"><i class="fa fa-calendar fa-fw"></i>Son 30 gün</a></li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>

<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
        <h4 class="page-header"><?php echo $currentStoreObj->name. '<span class="text-muted small"><em> ('.$timeInterval['startDate'].'</em></span><span class="fa fa-calendar fa-fw"> - </span><span class="text-muted small"><em>'.$timeInterval['endDate'].')</em></span> arası satışları'  ?></h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
    Sepet avarajı: <span id="basketAverage" name="basketAverage"></span><br><br>
    <table id="daily" class="display compact" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Ürün Kodu</th>
            <th>Kategori</th>
            <th>Fiyat</th>
            <th>Ödeme Şekli</th>
            <th>Satış Elemanı</th>
            <th>Satış Durumu</th>
            <th>Satış Tarihi</th>
        </tr>
        </thead>

        <tfoot>
        <tr>
            <th>ID</th>
            <th>Ürün Kodu</th>
            <th>Kategori</th>
            <th>Fiyat</th>
            <th>Ödeme Şekli</th>
            <th>Satış Elemanı</th>
            <th>Satış Durumu</th>
            <th>Satış Tarihi</th>
        </tr>
        </tfoot>
    </table>
</div>
<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

<script type="text/javascript">
    $(document).ready(function() {
        $('#daily').dataTable( {
            iDisplayLength: 1000,
            initComplete: function () {
                var api = this.api();

                api.columns().indexes().flatten().each( function ( i ) {
                    if (i!=2 && i!=4 && i!=5 && i!=6) {return true;}
                    var column = api.column( i );
                    var select = $('<select><option value=""></option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $(this).val();

                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );

                    column.data().unique().sort().each( function ( d, j ) {
                        if (i!=2 && i!=4 && i!=5 && i!=6) {return true;}
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );
            },

            "ajax": {
                "url":"/report/ajaxsales?store_id=<?php echo $storeId;?>&startDate=<?php echo $timeInterval['startDate'];?>&endDate=<?php echo $timeInterval['endDate'];?>",
                "dataSrc": function ( json ) {
                    $("#basketAverage").html(json.basketAverage.overall);
                    return json.data;
                }
            },

            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [ 5 ] }
            ],
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                if (aData[6] == 'iptal') {
                    $('td', nRow).closest('tr').css('background', '#f00');
                    return nRow;
                }
            },

            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                data = api.column( 3 ).data();
                total = data.length ?
                    data.reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    } ) :
                    0;

                // Total over this page
                data = api.column( 3, { page: 'current'} ).data();
                pageTotal = data.length ?
                    data.reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    } ) :
                    0;

                // Update header
                if (pageTotal > 0 && total > 0) {
                    $( api.column( 3 ).header() ).html(
                        Math.round(total*100)/100 +' TL Toplam <br/><span class="text-muted small"><em>('+ Math.round(pageTotal*100)/100 +' TL gösterilen)</em></spam>'
                    );
                }
            }
    } );


    } );

    function filterGlobal () {
        $('#daily').DataTable().search(
            $('#global_filter').val(),
            $('#global_regex').prop('checked'),
            $('#global_smart').prop('checked')
        ).draw();
    }

    function filterColumn ( i ) {
        $('#daily').DataTable().column( i ).search(
            $('#col'+i+'_filter').val(),
            $('#col'+i+'_regex').prop('checked'),
            $('#col'+i+'_smart').prop('checked')
        ).draw();
    }

    $(document).ready(function() {
        $('#daily').dataTable();

        $('input.global_filter').on( 'keyup click', function () {
            filterGlobal();
        } );

        $('input.column_filter').on( 'keyup click', function () {
            filterColumn( $(this).parents('tr').attr('data-column') );
        } );
    } );
</script>
<script>
    function setDates(day) {
        var currentDate = new Date();
        if (day) {
            currentDate.setDate(currentDate.getDate()-day);
            $("#datepicker").datepicker("setDate", currentDate);
            document.dateSubmit.submit();
        }
    }
    $(function() {
        var currentDate = new Date();
        currentDate.setDate(currentDate.getDate()-1);
        $('#datepicker').datepicker({
            firstDay: 1,
            showButtonPanel: true,
            showOtherMonths: true,
            autoSize: true,
            closeText: "Vazgeç",
            dayNamesMin: ['Pzr', 'Ptesi', 'Salı', 'Çar', 'Per', 'Cum', 'Ctesi'],
            monthNames: ['Ocak','Şubat','Mart','Nisan','Mayıs','Haziran','Temmuz','Ağustos','Eylül','Ekim','Kasım','Aralık'],
            dateFormat: 'yy-mm-dd'
        });
        <?php if (isset($timeInterval['startDate'])) {?>
            $("#datepicker").datepicker("setDate", '<?php echo $timeInterval['startDate'];?>');
        <?php } else {?>
            $("#datepicker").datepicker("setDate", currentDate);
        <?php } ?>

    });
    $(function() {
        var currentDate = new Date();
        $('#datepicker2').datepicker({
            firstDay: 1,
            showButtonPanel: true,
            showOtherMonths: true,
            autoSize: true,
            closeText: "Vazgeç",
            dayNamesMin: ['Pzr', 'Ptesi', 'Salı', 'Çar', 'Per', 'Cum', 'Ctesi'],
            monthNames: ['Ocak','Şubat','Mart','Nisan','Mayıs','Haziran','Temmuz','Ağustos','Eylül','Ekim','Kasım','Aralık'],
            dateFormat: 'yy-mm-dd'
        });
        <?php if (isset($timeInterval['endDate'])) {?>
            $("#datepicker2").datepicker("setDate", '<?php echo $timeInterval['endDate'];?>');
        <?php } else {?>
            $("#datepicker2").datepicker("setDate", currentDate);
        <?php } ?>
    });
</script>

</body>

</html>
