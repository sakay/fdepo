<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sakay
 * Date: 10/28/14
 * Time: 11:58 AM
 * To change this template use File | Settings | File Templates.
 */

?>
<div id="wrapper">

<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
<div class="navbar-header">
    <a class="navbar-brand" href="/">iFemaly</a>
</div>

<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a class="active" href="/report/index"><i class="fa fa-bar-chart-o fa-fw"></i> Rapor Anasayfa</a>
            </li>
            <?php
            foreach ($saleArr as $storeArr) {
                echo '<li><a href="/report/store?store_id='.$storeArr['store_id'].'"><i class="fa fa-line-chart fa-fw"></i> '.$storeArr['store_name'].' </a></li>';
            }
            ?>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>

<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
        <h4 class="page-header">Genel Bakış</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">

    <?php foreach ($saleArr as $index => $storeArr) {?>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-p<?php echo $index;?>">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-9 text-left">
                            <div class="huge"><?php echo $storeArr['total'];?></div>
                            <div><?php echo $storeArr['store_name'];?></div>
                        </div>
                        <div class="">
                            <i class="fa fa-try fa-5x"></i>
                        </div>
                    </div>
                </div>
                <a href="/report/store?store_id=<?php echo $storeArr['store_id'];?>">
                    <div class="panel-footer">
                        <span class="pull-left">Mağaza Detay</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    <?php }?>
</div>
<!-- /.row -->
<div class="row">
    <?php foreach ($saleArr as $index => $storeArr) { ?>
    <div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-thumbs-o-up fa-fw"></i> <?php echo $storeArr['store_name'];?>
                <span class="pull-right text-muted small"><em>Son 20 satış</em></span>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="list-group">
                    <?php foreach ($storeArr['last20'] as $lastSales) { ?>

                    <a href="#" class="list-group-item">
                        <i class="fa fa-<?php echo $lastSales->payment == 2? 'cc-visa':'money';?> fa-fw"></i>

                        <?php
                        if ($lastSales->price > 99) { echo '<b style="color:red">';}
                        echo number_format($lastSales->price,2)?> TL</b>
                        <span class="pull-right text-muted small"><em><?php echo $lastSales->timeAgo;?> önce</em></span>
                    </a>
                    <?php } ?>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel .chat-panel -->
    </div>
<?php }?>


</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

</body>

</html>
