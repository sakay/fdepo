<?php /* @var $this Controller */?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
    <meta name="robots" content="noindex,nofollow">

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.large.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/toastr.min.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/toastr.min.js"></script>
</head>

<body>

<div class="container" id="page">

    <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <a class="brand" href="/">&nbsp;&nbsp;&nbsp;iFemaly</a>
                <?php
                $pathInfo = Yii::app()->request->pathInfo;
                ?>
                <div class="nav-collapse">
                    <ul class="nav">
                        <li <?php echo $pathInfo == '' ? 'class="active"':'';?>><a href="/"><i class="icon-home icon-white"></i> Anasayfa </a></li>
                        <li <?php echo $pathInfo == 'site/report/index' ? 'class="active"':'';?>><a href="/report/index"><i class="icon-list-alt"></i> Raporlar </a></li>

                        <?php if (Yii::app()->session['role'] == "admin") { ?>
                        <li <?php echo $pathInfo == 'site/entry' ? 'class="active"':'';?>><a href="<?php echo Yii::app()->request->baseUrl;?>/site/entry"><i class="icon-arrow-down"></i>Ürün Girişi</a></li>
                        <li <?php echo $pathInfo == 'site/out' ? 'class="active"':'';?>><a href="<?php echo Yii::app()->request->baseUrl;?>/site/out"><i class="icon-arrow-up"></i>Ürün Çıkışı</a></li>
                        <li>
                            <form class="navbar-search pull-right" action="/site/search" method="post">
                                <input type="text" name="code" id="code" class="search-query span2" placeholder="Ürün Kodu Arama">
                            </form>
                        </li>
                        <?php }?>
                        <?php if (Yii::app()->user->id) { ?>
                            <li style="padding-left: 00px"><a href="<?php echo Yii::app()->request->baseUrl;?>/site/logout">Çıkış</a></li>
                        <?php } else { ?>
                            <li style="padding-left: 00px"><a href="/">Giriş</a></li>
                        <?php } ?>
                    </ul>
                </div><!-- /.nav-collapse -->
            </div><!-- /.container -->
        </div><!-- /.navbar-inner -->
    </div><!-- /.navbar -->

	<?php echo $content; ?>

	<div class="clear"></div>



</div><!-- page -->
<div id="footer">
    Copyright &copy; <?php echo date('Y'); ?> by Femaly.<br/>
    All Rights Reserved.<br/>
</div><!-- footer -->
</body>
</html>
