<?php /* @var $this Controller */?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />
    <meta name="robots" content="noindex,nofollow">

    <!-- Bootstrap Core CSS -->
    <link href="/report/css/bootstrap.min.css" rel="stylesheet">
    <link href="/report/css/plugins/jquery.dataTables.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/report/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="/report/css/plugins/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/report/css/sb-admin-2.css" rel="stylesheet">

    <script src="/report/js/jquery-1.11.0.js"></script>
    <script src="/report/js/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="/report/css/jquery-ui.min.css">

    <!-- Bootstrap Core JavaScript -->
    <script src="/report/js/bootstrap.min.js"></script>
    <script src="/report/js/plugins/dataTables/jquery.dataTables.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="/report/js/plugins/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="/report/js/sb-admin-2.js"></script>



    <!-- Custom Fonts -->
    <link href="/report/font-awesome-4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/toastr.min.js"></script>
</head>

<body>

	<?php echo $content; ?>

</body>
</html>
