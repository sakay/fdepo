<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sakay
 * Date: 10/3/13
 * Time: 1:40 PM
 * To change this template use File | Settings | File Templates.
 */

class Photo extends CActiveRecord
{
    public $id;
    public $product_id;
    public $image;
    public $status;

    public function rules()
    {
        return array(
            array('product_id, image', 'required'),
            //array('code', 'unique','on'=>'insert', 'message'=>'<b style="color: RED">Urun Kodu Kullanilmakta!!</b>'),
        );
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'photos';
    }

    public function primaryKey()
    {
        return 'id';
        // For composite primary key, return an array like the following
        // return array('pk1', 'pk2');
    }

    public function productPhotos($product_id) {
        $photoArr = self::model()->findAll('product_id=:product_id', array(':product_id'=>$product_id));
        $returnArr = array();
        foreach ($photoArr as $val) {
            $returnArr['images'][$val->id] = $val->image;
        }
        return $returnArr;
    }
}