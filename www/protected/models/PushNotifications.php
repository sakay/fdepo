<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sakay
 * Date: 11/29/14
 * Time: 3:00 AM
 * To change this template use File | Settings | File Templates.
 */

class PushNotifications {

    public function pushNotification($title, $message){
        curl_setopt_array($ch = curl_init(), array(
            CURLOPT_URL => "https://api.pushover.net/1/messages.json",
            CURLOPT_RETURNTRANSFER=>1,
            CURLOPT_POSTFIELDS => array(
                "token" => "aF5J3HywaP9hLK82r1KtZeRBNoygPg",
                "user" => "g7GEwKLQmJsweR8GqbT3iQBet8cDAA",
                "title" => $title,
                "message" => $message,
                "sound" => "cashregister",
            )
        ));
        curl_exec($ch);
        curl_close($ch);
    }
}