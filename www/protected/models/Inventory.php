<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sakay
 * Date: 10/3/13
 * Time: 1:40 PM
 * To change this template use File | Settings | File Templates.
 */

class Inventory extends CActiveRecord
{
    public $id;
    public $product_id;
    public $store_id;
    public $quantity;

    public function rules()
    {
        return array(
        );
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'store_inventory';
    }

    public function primaryKey()
    {
        return 'id';
        // For composite primary key, return an array like the following
        // return array('pk1', 'pk2');
    }

    public function allInventory() {
        $allInventory = self::model()->findAll();
        $returnArr = array();
        foreach ($allInventory as $val) {
            $returnArr[$val->id]['product_id'] = $val->product_id;
            $returnArr[$val->id]['store_id'] = $val->store_id;
            $returnArr[$val->id]['quantity'] = $val->quantity;
        }
        return $returnArr;
    }

    public function productInventory($product_id) {
        $photoArr = self::model()->findAll('product_id=:product_id', array(':product_id'=>$product_id));
        $returnArr = array();
        foreach ($photoArr as $val) {
            $returnArr[$val->store_id]['id'] = $val->id;
            $returnArr[$val->store_id]['product_id'] = $val->product_id;
            $returnArr[$val->store_id]['store_id'] = $val->store_id;
            $returnArr[$val->store_id]['quantity'] = $val->quantity;
        }
        return $returnArr;
    }

    public function findInventoryByProductIDstoreID($productId,$storeId) {
        return self::model()->find('product_id=:product_id and store_id=:store_id', array(':product_id'=>$productId,':store_id'=>$storeId));
    }

    public function setId($ID) {
        $this->id = $ID;
    }
    public function getId() {
        return $this->id;
    }
    public function setStoreId($storeID) {
        $this->store_id = $storeID;
    }
    public function getStoreId() {
        return $this->store_id;
    }
    public function setProductId($productId) {
        $this->product_id = $productId;
    }
    public function getProductId() {
        return $this->product_id;
    }
    public function getQuantity() {
        return $this->quantity;
    }
    public function setQuantity($quantity) {
        $this->quantity = $quantity;
    }
}