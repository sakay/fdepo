<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sakay
 * Date: 10/3/13
 * Time: 1:40 PM
 * To change this template use File | Settings | File Templates.
 */

class Categories extends CActiveRecord
{
    public $id;
    public $category_name;

    public function rules()
    {
        return array(
        );
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'product_category';
    }

    public function primaryKey()
    {
        return 'id';
        // For composite primary key, return an array like the following
        // return array('pk1', 'pk2');
    }

    public function allCategories() {
        $photoArr = self::model()->findAll();
        $returnArr = array();
        foreach ($photoArr as $val) {
            $returnArr[$val->id] = $val->category_name;
        }
        return $returnArr;
    }
}