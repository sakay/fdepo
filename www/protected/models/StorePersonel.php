<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sakay
 * Date: 10/3/13
 * Time: 1:40 PM
 * To change this template use File | Settings | File Templates.
 */

class StorePersonel extends CActiveRecord
{
    public $id;
    public $store_id;
    public $person_name;
    public $status;

    public function rules()
    {

    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'store_personel';
    }

    public function primaryKey()
    {
        return 'id';
    }
}