<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sakay
 * Date: 10/3/13
 * Time: 1:40 PM
 * To change this template use File | Settings | File Templates.
 */

class EntryForm extends CActiveRecord
{
    public $id;
    public $category_id;
    public $code;
    public $title;
    public $description;
    public $buy_price;
    public $sale_price1;
    public $sale_price2;
    public $picture_id;
    public $quantity;
    public $size;
    public $color;


    public function rules()
    {
        return array(
            array('category_id, code, title,quantity', 'required','on' => 'insert'),
            array('code', 'unique', 'message'=>'<b style="color: RED">Ürün Kodu Kullanılmakta !</b>','on'=>'insert'),
        );
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'products';
    }

    public function primaryKey()
    {
        return 'id';
        // For composite primary key, return an array like the following
        // return array('pk1', 'pk2');
    }

    public function lastEntry() {
        return self::model()->find(array(
            'select'=>'code,title,id,buy_price',
            'order'=>'id DESC',
            'limit'=>1,
        ));
    }

    public function findProductByCode($product_code) {
        return self::model()->find('code=:code', array(':code'=>$product_code));
    }

    public function findAllProducts($limit = 50, $offset = 0) {
        $criteria = new CDbCriteria(array(
            'order' => 'id ASC',
            'limit' => $limit,
            'offset' => $offset
        ));
        return self::model()->findAll($criteria);
    }

    public function findAllProductsByParams($params, $limit = 50, $offset = 0) {

        $conditionQry = '';
        if (isset($params['category_id']) && $params['category_id'] != 'all') {
            $condition[] = 'category_id = '.$params['category_id'];
        }
        if (isset($params['code']) && !empty($params['code'])) {
            $condition[] = "code like '".$params['code']."%'";
        }
        if (isset($condition)) {
            $conditionQry = implode(" and ",$condition);
        }
        $criteria = new CDbCriteria(array(
            'condition' => $conditionQry,
            'order' => 'id ASC',
            'limit' => $limit,
            'offset' => $offset
        ));

        return self::model()->findAll($criteria);
    }
}