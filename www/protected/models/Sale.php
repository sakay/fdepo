<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sakay
 * Date: 10/3/13
 * Time: 1:40 PM
 * To change this template use File | Settings | File Templates.
 */

class Sale extends CActiveRecord
{
    public $id;
    public $code;
    public $product_category;
    public $price;
    public $payment;
    public $store_id;
    public $personel_id;
    public $discount;
    public $basket_id;
    public $idate;
    public $total;
    public $timeAgo;

    public function rules()
    {
        return array(
            array('code, price, payment', 'required','on' => 'insert')
        );
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'sale';
    }

    public function primaryKey()
    {
        return 'id';
    }

    public function setStoreID($storeId) {
        $this->store_id = $storeId;
    }
    public function getStoreID() {
        return $this->store_id;
    }
    public function todayStartEnd() {
        $hour = intval(date("H"));
        if ($hour < 6) {
            $startDate = date("Y-m-d", time() - 60 * 60 * 24). " 06:00:00";
            $endDate = date("Y-m-d") . " 06:00:00";
        } else {
            $startDate = date("Y-m-d"). " 06:00:00";
            $endDate = date("Y-m-d") . " 23:59:59";
        }
        return array('startDate'=>$startDate,'endDate'=>$endDate);
    }
    public function dailyTotalSale() {
        $store_id = $this->getStoreID();

        $criteria = new CDbCriteria(array(
            'condition' => 'store_id='.$store_id
        ));
        $today = $this->todayStartEnd();
        $criteria->select = 'sum(price) as total,payment,store_id';
        $criteria->addBetweenCondition('idate', $today['startDate'], $today['endDate']);
        $result = self::model()->findAll($criteria);
        return $result[0]['total'];
    }

    // $timeInterval: $timeInterval['startDate'],$timeInterval['endDate']
    public function dailySales($timeInterval = null) {
        $store_id = $this->getStoreID();
        $criteria = new CDbCriteria(array(
            'condition' => 'store_id='.$store_id
        ));
        if (!$timeInterval['startDate'] || !$timeInterval['endDate']) {
            $timeInterval = $this->todayStartEnd();
        }
        $criteria->addBetweenCondition('idate', $timeInterval['startDate'], $timeInterval['endDate']);
        $result = self::model()->findAll($criteria);
        return $result;
    }

    public function last20Sales($store_id) {
        $resultObj = self::findAllByAttributes(array('store_id'=>$store_id),array('order'=>'id DESC','limit'=>'20'));
        foreach ($resultObj as &$objVal) {
            $objVal->timeAgo = $this->someTimeAgo($objVal->idate);
        }
        return $resultObj;
    }
    public function someTimeAgo($datetime) {
        $time = strtotime($datetime);
        $time = time() - $time; // to get the time since that moment
        $tokens = array (
            31536000 => 'yıl',
            2592000 => 'ay',
            604800 => 'hafta',
            86400 => 'gün',
            3600 => 'saat',
            60 => 'dk',
            1 => 'saniye'
        );

        foreach ($tokens as $unit => $text) {
            if ($time < $unit) continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits.' '.$text;
        }
        return false;
    }
}