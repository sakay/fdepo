<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sakay
 * Date: 10/3/13
 * Time: 1:40 PM
 * To change this template use File | Settings | File Templates.
 */

class Store extends CActiveRecord
{
    public $id;
    public $name;
    public $use_price;

    public function rules()
    {
        return array(
        );
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'store';
    }

    public function primaryKey()
    {
        return 'id';
        // For composite primary key, return an array like the following
        // return array('pk1', 'pk2');
    }

    public function allStores() {
        $allStores = self::model()->findAll();
        $returnArr = array();
        foreach ($allStores as $val) {
            $returnArr[$val->id] = $val->name;
        }
        return $returnArr;
    }

}