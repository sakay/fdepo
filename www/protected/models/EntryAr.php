<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sakay
 * Date: 10/3/13
 * Time: 1:40 PM
 * To change this template use File | Settings | File Templates.
 */

class EntryAr extends CActiveRecord
{
    public $id;
    public $category_id;
    public $code;
    public $title;
    public $description;
    public $buy_price;
    public $sale_price1;
    public $sale_price2;
    public $picture_id;
    public $quantity;
    public $size;
    public $color;


    public function rules()
    {
        return array(
            array('category_id, code, title,quantity', 'required'),
            array('code', 'unique','on'=>'insert', 'message'=>'Urun Kodu Kullanilmakta'),
        );
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'products';
    }

    public function primaryKey()
    {
        return 'id';
        // For composite primary key, return an array like the following
        // return array('pk1', 'pk2');
    }
}