<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sakay
 * Date: 10/3/13
 * Time: 1:40 PM
 * To change this template use File | Settings | File Templates.
 */

class User extends CActiveRecord
{
    public $id;
    public $user_name;
    public $user_pass;
    public $role;
    public $store;
    public $status;

    public function rules()
    {
        return array(
        );
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'duser';
    }

    public function primaryKey()
    {
        return 'id';
    }
}