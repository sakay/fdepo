<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sakay
 * Date: 10/3/13
 * Time: 1:40 PM
 * To change this template use File | Settings | File Templates.
 */

class ProductCategory extends CActiveRecord
{
    public $id;
    public $category_name;
    public $status;

    public function rules()
    {

    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'product_category';
    }

    public function primaryKey()
    {
        return 'id';
    }
}