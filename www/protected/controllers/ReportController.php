<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sakay
 * Date: 10/28/14
 * Time: 11:49 AM
 * To change this template use File | Settings | File Templates.
 */

class ReportController extends Controller{

    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'list' and 'show' actions
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('index','Store','AjaxSales'),
                'expression' => 'Yii::app()->session[\'role\'] == "admin"',
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('index','Store','AjaxSales'),
                'expression' => 'Yii::app()->session[\'role\'] == "store"',
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function actionIndex()
    {
        $this->layout = 'reportLayout';
        $SALE = new Sale;
        $Store = new Store;
        $storeObj = $Store->findAllByAttributes(array('type'=>1));
        $saleArr = array();
        foreach ($storeObj as $ind => $store) {
            if (Yii::app()->session['role'] != 'admin' && Yii::app()->session['store'] != $store->id) {
                continue;
            }
            $saleArr[$ind]['store_id'] = $store->id;
            $saleArr[$ind]['store_name'] = $store->name;
            $SALE->setStoreID($store->id);
            $saleArr[$ind]['total'] = number_format($SALE->dailyTotalSale(),2);
            $saleArr[$ind]['last20'] = $SALE->last20Sales($store->id);

        }
        $this->render('index',array('saleArr' => $saleArr));
    }
    public function actionStore() {
        $this->layout = 'reportLayout';
        $SALE = new Sale;
        $Store = new Store;
        if (Yii::app()->session['role'] != 'admin' && Yii::app()->session['store'] != $_GET['store_id']) {
            return false;
        }
        if (Yii::app()->session['role'] == 'admin') {
            $storeObj = $Store->findAllByAttributes(array('type'=>1));
        } else {
            $storeObj = $Store->findAllByAttributes(array('type'=>1,'id'=>Yii::app()->session['store']));
        }
        $SALE->setStoreID($_GET['store_id']);

        if (!isset($_POST['start']) && !isset($_POST['end'])) {
            $timeInterval['startDate'] = date("Y-m-d"). " 06:00:00";
            $timeInterval['endDate'] = date("Y-m-d H:i:s");
        } else {
            $timeInterval['startDate'] = $_POST['start']. " 06:00:00";;
            $timeInterval['endDate'] = $_POST['end']. " 06:00:00";;
        }

        $saleObj = $SALE->dailySales($timeInterval);
        $this->render('store',array('saleObj' => $saleObj,'storeObj' => $storeObj,'storeId'=>$_GET['store_id'],'timeInterval'=>$timeInterval));
    }

    public function actionAjaxSales() {
        $SALE = new Sale;
        $PERSONEL = new StorePersonel;
        $SALE->setStoreID($_GET['store_id']);
        if (!isset($_GET['startDate']) && !isset($_GET['endDate'])) {
            $timeInterval['startDate'] = date("Y-m-d", time() - 60 * 60 * 24). " 06:00:00";
            $timeInterval['endDate'] = date("Y-m-d H:i:s");
        } else {
            $timeInterval['startDate'] = $_GET['startDate'];
            $timeInterval['endDate'] = $_GET['endDate'];
        }
        $saleObj = $SALE->dailySales($timeInterval);
        $personelObj = $PERSONEL->findAllByAttributes(array('store_id'=>$_GET['store_id']));
        $personelArr = array();
        foreach ($personelObj as $val) {
            $personelArr[$val->id] = $val->person_name;
        }

        $saleArr = array();
        $paymentArr[1] = 'Nakit';
        $paymentArr[2] = 'Kredi Kartı';

        $productCategory = new ProductCategory;
        $allProductCategory = $productCategory->findAll();
        $productCategoryArr = array();
        foreach ($allProductCategory as $val) {
            $productCategoryArr[$val->id] = $val->category_name;
        }
        $basketArr = array();
        $personelSaleTotal = array();
        foreach ($saleObj as $ind => $objVal) {
            $saleArr['data'][$ind][] = $objVal->id;
            $saleArr['data'][$ind][] = $objVal->code;
            $saleArr['data'][$ind][] = isset($productCategoryArr[$objVal->product_category])?$productCategoryArr[$objVal->product_category]:'-';
            $saleArr['data'][$ind][] = $objVal->price;
            $saleArr['data'][$ind][] = isset($paymentArr[$objVal->payment])?$paymentArr[$objVal->payment]:'-';
            //TODO
            $personelName = isset($personelArr[$objVal->personel_id])?$personelArr[$objVal->personel_id]:'-';
            $saleArr['data'][$ind][] = $personelName;
            $saleArr['data'][$ind][] = $objVal->status == 1 ? 'Aktif':'iptal';
            $saleArr['data'][$ind][] = $objVal->idate;
            $basketArr['overall'][$objVal->basket_id] = $objVal->basket_id;
            if (isset($objVal->basket_id)) {
                $basketArr['personel'][$personelName][$objVal->basket_id] = $objVal->basket_id;
                $personelSaleTotal[$personelName] = isset($personelSaleTotal[$personelName])?$personelSaleTotal[$personelName]+= 1:1;
            }


        }
        if (empty($saleArr)) {
            $saleArr['data'][0][] = 'Satış Yok';
            $saleArr['data'][0][] = 'Satış Yok';
            $saleArr['data'][0][] = 'Satış Yok';
            $saleArr['data'][0][] = 'Satış Yok';
            $saleArr['data'][0][] = 'Satış Yok';
            $saleArr['data'][0][] = 'Satış Yok';
            $saleArr['data'][0][] = 'Satış Yok';
            $saleArr['data'][0][] = 'Satış Yok';
        }
        $tmpPerspnelAverage = array();
        if (isset($basketArr['personel'])) {
            foreach ($basketArr['personel'] as $name => $val) {
                $tmpPerspnelAverage['basketAverage']['personel'][] = $name." ".number_format($personelSaleTotal[$name] / count($val),1);
            }

            $saleArr['basketAverage']['overall'] = number_format(count($saleObj) / count($basketArr['overall']),1).
                " (".implode(", ",$tmpPerspnelAverage['basketAverage']['personel']).")";
        } else {
            $saleArr['basketAverage']['overall'] = '';
        }

        echo json_encode($saleArr);
        exit;
    }
}