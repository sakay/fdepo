<?php

class SiteController extends Controller
{

    public function init() {
        // filter out garbage requests
        $uri = Yii::app()->request->requestUri;
        if (strpos($uri, 'favicon') || strpos($uri, 'robots')) {
            $this->redirect(Yii::app()->request->baseUrl.'/site/index');
        }
    }

    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'list' and 'show' actions
                'actions'=>array('index','logout','images','uploads','thumbs','hadleExcelAjax'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('entry','out','index','cancel','sale','excel','report','search','AjaxCodeChecker','AjaxOutCheck','edit','DeletePicture','hadleExcelAjax'),
                'expression' => 'Yii::app()->session[\'role\'] == "admin"',
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('index','sale','cancel','satisRapor','entry','AjaxCodeChecker','search','AjaxOutCheck','edit','DeletePicture'),
                'expression' => 'Yii::app()->session[\'role\'] == "store"',
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function actionCancel() {
        $model=new EntryForm;
        $saleObj = array();
        $err = false;
        $success = false;
        if (isset($_GET['sale_id'])) {
            $saleModel = new Sale;
            $saleObj = $saleModel->findByPk($_GET['sale_id']);
            if (!isset($saleObj->id)) {
                $err = 'not_found';
            } else if ($saleObj->store_id != $_SESSION['store']) {
                $err = 'other_store';
                unset($saleObj);
                $saleObj = array();
            }
            // satis iptali
            if ($err == false && $saleObj->status == 1 && isset($_GET['cancel']) && $_GET['cancel'] == 1) {
                $productInfo = EntryForm::model()->findProductByCode($saleObj->code);
                $saleModel->updateByPk($saleObj->id,array("status"=> -1 ));
                $saleObj = $saleModel->findByPk($_GET['sale_id']);
                $INV = new Inventory;
                if ($inventoryObj = $INV->findInventoryByProductIDstoreID($productInfo->id,$saleObj->store_id)) {
                    $INV->updateByPk($inventoryObj->id,array("quantity"=> $inventoryObj->quantity - 1));
                }
                $success = true;
            } else if ($err == false && $saleObj->status == -1 && isset($_GET['cancel']) && $_GET['cancel'] == -1) {
                // iptalin iptali
                $productInfo = EntryForm::model()->findProductByCode($saleObj->code);
                $saleModel->updateByPk($saleObj->id,array("status"=> 1 ));
                $saleObj = $saleModel->findByPk($_GET['sale_id']);
                $INV = new Inventory;
                if ($inventoryObj = $INV->findInventoryByProductIDstoreID($productInfo->id,$saleObj->store_id)) {
                    $INV->updateByPk($inventoryObj->id,array("quantity"=> $inventoryObj->quantity + 1));
                }
                $success = true;
            }

        }
        $this->render('cancel',array('model'=>$model,'saleObj'=>$saleObj,'err'=>$err, 'success' => $success));

    }
    public function actionSale() {
        $success = false;
        $productQntErrString = '';
        $uniqueBasketValue = md5(date('YmdHis'));
        if (isset($_POST['product_code']) && isset($_POST['pcode'])) {

            // Check if submited product quantites in store inventory
            $productQuantityError = array();
            $productQuantity = array();

            foreach ($_POST['pcode'] as $index => $val ) {
                $productObj =  EntryForm::model()->findProductByCode($val);
                $inventoryModel = new Inventory;
                $productInventory = $inventoryModel->find('product_id=:product_id and store_id=:store_id', array(':product_id'=>$productObj['id'],':store_id'=>Yii::app()->session['store']));

                if (isset($productInventory->quantity) && $productInventory->quantity > 0) {
                    $productQuantity[$productObj->code] = isset($productQuantity[$productObj->code])?$productQuantity[$productObj->code] + 1:1;
                }
                if ($productQuantity[$productObj->code] > $productInventory->quantity) {
                    $productQuantityError[$productObj->code] = $productInventory->quantity;
                }
            }

            if (!count($productQuantityError) > 0) {
                $total = 0;
                foreach ($_POST['pcode'] as $index => $val ) {
                    $productObj =  EntryForm::model()->findProductByCode($val);
                    $inventoryModel = new Inventory;
                    $productInventory = $inventoryModel->find('product_id=:product_id and store_id=:store_id', array(':product_id'=>$productObj['id'],':store_id'=>Yii::app()->session['store']));

                    if (isset($productInventory->quantity) && $productInventory->quantity > 0) {
                        $productInventory->product_id = $productObj->id;
                        $productInventory->quantity = ($productInventory->quantity - 1);
                        $productInventory->save();
                    } else {
                        continue;
                    }
                    $saleModel = new Sale;
                    $saleModel->code = $val;
                    $saleModel->product_category = $productObj->category_id;
                    $saleModel->price = $_POST['price'][$index];
                    $saleModel->payment = $_POST['paymentMethod'];
                    $saleModel->personel_id = $_POST['personelID'];
                    $saleModel->store_id = Yii::app()->session['store'];
                    $saleModel->discount = 0;
                    $saleModel->basket_id = $uniqueBasketValue;
                    $saleModel->idate = date("Y-m-d H:i:s");
                    if ($saleModel->save()) {
                        $total = $total + $_POST['price'][$index];
                        $success = true;
                    } else {
                        $success = -1;
                    }
                }
                if ($success == true && $total >= 100) {
                    $PUSH = new PushNotifications;
                    $Store = new Store;
                    $storeObj = $Store->findAllByAttributes(array('type'=>1,'id'=>Yii::app()->session['store']));
                    $PUSH->pushNotification("QQ - ".$storeObj[0]->name,$total." TL");
                }
            } else {
                foreach ($productQuantityError as $code => $qnt) {
                    // TODO LOG here
                    $productQntErrString .= 'Ürün: ' . $code .' Toplam Adet: '.$qnt.'<br>';
                }
            }
        }

        $saleModel = new Sale;
        $storePersonel = new StorePersonel;
        $storePersonelObj = $storePersonel->findAllByAttributes(array('store_id'=>Yii::app()->session['store']));
        $this->render('sale',array('model'=>$saleModel, 'success' => $success,'productQntErrString'=>$productQntErrString,'storePersonel'=>$storePersonelObj));
    }


	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		//$this->render('index');

        if (Yii::app()->user->id) {
            $this->render('index',array('login'=>true));
            exit;
        }

        $model=new LoginForm;

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login()) {
                $this->redirect(Yii::app()->user->returnUrl);
            }
        }

        $this->render('index',array('model'=>$model,'login'=>false));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

    public function actionEdit() {

        $model = EntryForm::model()->findByPk($_GET['product_id']);


        $photo = new Photo;
        $success_code = '';

        if(isset($_POST['EntryForm'])) {
            $model->attributes = $_POST['EntryForm'];
            $model->category_id = $_POST['EntryForm']['category_id'];
            $model->code = $_POST['EntryForm']['code'];
            $model->title = $_POST['EntryForm']['title'];
            $model->description = $_POST['EntryForm']['description'];
            if (!isset($_POST['EntryForm']['buy_price']) || empty($_POST['EntryForm']['buy_price'])) {
                $model->buy_price = 0.00;
            } else {
                $model->buy_price = $_POST['EntryForm']['buy_price'];
            }
            if (!isset($_POST['EntryForm']['sale_price1']) || empty($_POST['EntryForm']['sale_price1'])) {
                $model->sale_price1 = 0.00;
            } else {
                $model->sale_price1 = $_POST['EntryForm']['sale_price1'];
            }
            if (!isset($_POST['EntryForm']['sale_price2'])  || empty($_POST['EntryForm']['sale_price2'])) {
                $model->sale_price2 = 0.00;
            } else {
                $model->sale_price2 = $_POST['EntryForm']['sale_price2'];
            }
            $model->size = $_POST['EntryForm']['size'];
            $model->color = $_POST['EntryForm']['color'];


            if ($model->save()) {
                if (isset($_FILES['dosya']['name'][0])) {
                    $path = realpath( Yii::app( )->getBasePath( )."/../images/uploads/original" )."/";
                    $thumbPath = realpath( Yii::app( )->getBasePath( )."/../images/uploads/thumbs" )."/";
                    $dosya_sayi=count($_FILES['dosya']['name']);
                    for($i=0;$i<$dosya_sayi;$i++){
                        if(!empty($_FILES['dosya']['name'][$i])){
                            move_uploaded_file($_FILES['dosya']['tmp_name'][$i],$path."/".$_FILES['dosya']['name'][$i]);
                            $this->resim_yukle($path."/".$_FILES['dosya']['name'][$i],$thumbPath.$_FILES['dosya']['name'][$i],240,320,100);
                            $command = Yii::app()->db->createCommand();
                            $command->insert('photos', array(
                                'product_id'=>$model->id ,
                                'image'=>$_FILES['dosya']['name'][$i] ,
                                'status'=>'1'));
                        }
                    }
                }
                $success_code = $model->code;
            }
        }
        $productObj = $model->findByPk($_GET['product_id']);
        $photoArr = $photo->productPhotos($_GET['product_id']);
        $categoryModel = new Categories;

        $this->render('product_entry',array('model'=>$model,'allCategories'=>$categoryModel->allCategories(), 'edit'=>1, 'success_code'=>$success_code,'lastRow'=>'','productObj'=>$productObj,'photoArr' => $photoArr));

    }

    public function actionOut()
    {
        $model=new EntryForm;
        $storeModel = new Store;
        $productInventory = array();
        $inventoryModel = new Inventory;
        $err = false;$success = false;$hideInputs = false;
        if (isset($_GET['submit'])) {
            $productObj = Yii::app()->db->createCommand()
                ->select('id')
                ->from('products')
                ->where('code=:code', array(':code'=>$_GET['code']))
                ->queryRow();

            if (isset($productObj['id'])) {
                foreach ($_GET['store'] as $storeId => $quantity) {
                    if ($quantity >= 0) {
                        $inventoryModel = new Inventory;
                        $inventoryModel = $inventoryModel->find('product_id=:product_id and store_id=:store_id', array(':product_id'=>$productObj['id'],':store_id'=>$storeId));
                        if (!isset($inventoryModel)) {
                            $inventoryModel = new Inventory;
                        }
                        $inventoryModel->product_id = $productObj['id'];
                        $inventoryModel->store_id = $storeId;
                        $inventoryModel->quantity = $quantity;
                        if ($inventoryModel->save()) {
                            $success = true;
                            $hideInputs = true;
                        }
                    }
                }
            }
        }
        if (isset($_GET['code'])) {
            $productObj = Yii::app()->db->createCommand()
                ->select('id')
                ->from('products')
                ->where('code=:code', array(':code'=>$_GET['code']))
                ->queryRow();
            if (isset($productObj['id'])) {
                $productInventory = $inventoryModel->productInventory($productObj['id']);
            } else {
                $err = true;
            }
        }

        $this->render('product_out',array('model'=>$model, 'hideInputs'=>$hideInputs, 'allStores'=>$storeModel->allStores(),'inventory'=>$productInventory, 'err' => $err, 'success' => $success));
    }

	public function actionEntry()
	{
		$model=new EntryForm;
        $success_code = '';
        $inventoryModel = new Inventory;


		if(isset($_POST['EntryForm']))
		{
            $model->attributes=$_POST['EntryForm'];
            $model->category_id = $_POST['EntryForm']['category_id'];
            $model->code = $_POST['EntryForm']['code'];
            $model->title = $_POST['EntryForm']['title'];
            $model->description = $_POST['EntryForm']['description'];
            if (empty($_POST['EntryForm']['buy_price'])) {
                $model->buy_price = null;
            } else {
                $model->buy_price = str_replace(",",".",$_POST['EntryForm']['buy_price']);
            }
            if (empty($_POST['EntryForm']['sale_price1'])) {
                $model->sale_price1 = null;
            } else {
                $model->sale_price1 = str_replace(",",".",$_POST['EntryForm']['sale_price1']);
            }
            if (empty($_POST['EntryForm']['sale_price2'])) {
                $model->sale_price2 = null;
            } else {
                $model->sale_price2 = str_replace(",",".",$_POST['EntryForm']['sale_price2']);
            }


            $model->size = $_POST['EntryForm']['size'];
            $model->color = $_POST['EntryForm']['color'];

            if ($model->save()) {
                $inventoryModel->product_id = $model->id;
                $inventoryModel->quantity = $_POST['EntryForm']['quantity'];
                $inventoryModel->store_id = $_POST['Store']['id'];
                $inventoryModel->save();
                $success_code = $_POST['EntryForm']['code'];
                if (isset($_FILES['dosya']['name'][0])) {
                    $path = realpath( Yii::app( )->getBasePath( )."/../images/uploads/original" )."/";
                    $thumbPath = realpath( Yii::app( )->getBasePath( )."/../images/uploads/thumbs" )."/";
                    $dosya_sayi=count($_FILES['dosya']['name']);
                    for($i=0;$i<$dosya_sayi;$i++){
                        if(!empty($_FILES['dosya']['name'][$i])){
                            move_uploaded_file($_FILES['dosya']['tmp_name'][$i],$path."/".$_FILES['dosya']['name'][$i]);
                            $this->resim_yukle($path."/".$_FILES['dosya']['name'][$i],$thumbPath.$_FILES['dosya']['name'][$i],240,320,100);
                            $command = Yii::app()->db->createCommand();
                            $command->insert('photos', array(
                                'product_id'=>$model->id ,
                                'image'=>$_FILES['dosya']['name'][$i] ,
                                'status'=>'1'));
                        }
                    }
                }
            } else {
                $model->addError('code','HATA');
            }
        }

        $lastRow = $model->lastEntry();
        $photo = new Photo;
        $storeModel = new Store;
        $photoArr = $photo->productPhotos($lastRow->id);
        $storeArr = $storeModel->allStores();
        $categoryModel = new Categories;

		$this->render('product_entry',array('model'=>$model,'storeModel'=>$storeModel,'storeArr'=>$storeArr,'allCategories'=>$categoryModel->allCategories(),'success_code'=>$success_code,'photoArr' => $photoArr,'lastRow'=>$lastRow));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

    public function actionAjaxCodeChecker() {
        $result = Yii::app()->db->createCommand()
            ->select('id')
            ->from('products')
            ->where('code=:code', array(':code'=>$_POST['EntryForm']['code']))
            ->queryRow();
        if ($result) {
            echo '1';
        } else {
            echo '-1';
        }
    }

    public function actionSearch() {
        $code = $_POST['code'];
        $result = Yii::app()->db->createCommand()
            ->select('id')
            ->from('products')
            ->where('code=:code', array(':code'=>$code))
            ->queryRow();

        if (isset($result['id'])) {
            $this->redirect(Yii::app()->request->baseUrl.'/site/edit?product_id='.$result['id']);
        } else {
            ?><script>alert('Geçersiz ürün kodu');history.back();</script><?php
        }

    }
    public function actionAjaxOutCheck() {

        $inventoryModel = new Inventory;
        $productObj = EntryForm::model()->findByAttributes(array('code'=>$_GET['product_code']));
        $productCategory = new ProductCategory;
        $allProductCategory = $productCategory->findAllByAttributes(array('status'=>1));
        $productCategoryArr = array();
        foreach ($allProductCategory as $val) {
            $productCategoryArr[$val->id] = $val->category_name;
        }

        $result = array();
        if (isset($productObj->id)) {
            $productInventory = $inventoryModel->productInventory($productObj->id);
            if (isset($productInventory[Yii::app()->session['store']]['quantity'])
                && $productInventory[Yii::app()->session['store']]['quantity'] > 0) {
                if (Yii::app()->session['use_price'] == 2) {
                    $result['price'] = !empty($productObj->sale_price2)?$productObj->sale_price2:0.00;
                } else {
                    $result['price'] = !empty($productObj->sale_price1)?$productObj->sale_price1:0.00;
                }
                $result['product_code'] = $productObj->code;
                $result['category_id'] = $productObj->category_id;
                $result['category_name'] = isset($productCategoryArr[$productObj->category_id])?$productCategoryArr[$productObj->category_id]:'-';

            } else {
                $result['err'] = -2;
            }
        } else {
            $result['err'] = -1;
        }
        echo json_encode($result);
        exit;
    }

    public function actionReport() {
        $result = Yii::app()->db->createCommand()
            ->select('category_id, COUNT(*) as total')
            ->from('products')
            ->group('category_id')
            ->queryAll();
        $categoryModel=new Categories;

        $categoryArr = $categoryModel->allCategories();
        $total = 0;

        $categoryCountArr = array();
        foreach ($result as $val) {
            if (isset($categoryArr[$val['category_id']])) {
                $categoryCountArr[$categoryArr[$val['category_id']]]['count'] = $val['total'];
                $total += $val['total'];
            }
        }

        $this->render('report',array('categoryCountArr'=>$categoryCountArr,'total'=>$total));
    }

    public function actionDeletePicture() {

        $photoId = $_GET['id'];
        $image   = $_GET['image'];
        if (!$photoId) {
            die('-1');
        }
        $sql = "DELETE FROM photos where id = '".$photoId."'";
        Yii::app()->db->createCommand($sql)->execute();
        //$path = realpath( Yii::app( )->getBasePath( )."/../images/uploads/original" )."/";
        //unlink($path.$image);
        die($photoId);
    }

    public function actionExcel() {

        $params['category_id'] = isset($_GET['category_id'])?$_GET['category_id']:'all';
        $params['code'] = isset($_GET['code'])?$_GET['code']:'';

        $model=new EntryForm;
        $offset = 0;
        $limit = 30;

        $currentPage = 1;
        if (isset($_GET['sf'])) {
            $currentPage = $_GET['sf'];
            $offset = ($currentPage-1) * $limit;
        }

        $productObj = $model->findAllProductsByParams($params, $limit, $offset);
        $totalObj = $model->findAllProductsByParams($params, 100000, 0);

        $categoryModel = new Categories;
        $categoryArr = $categoryModel->allCategories();

        $rowsArray = array();
        $photo = new Photo;
        foreach ($productObj as $val) {
            $photoArr = $photo->productPhotos($val->id);
            if (isset($photoArr['images'])) {
                $arrVals = array_values($photoArr['images']);;
                $photoUrl = '/images/uploads/thumbs/'.$arrVals[0];
            } else {
                $photoUrl = '/images/fem50x50.jpg';
            }

            $rowsArray[] = array($val->id,
                isset($categoryArr[$val->category_id])?$categoryArr[$val->category_id]:'',
                $val->code,
                $val->title,
                $val->description,
                $val->buy_price,
                $val->sale_price1,
                $val->sale_price2,
                $val->quantity,$val->size,$val->color,'
                <a href="'.$photoUrl.'" data-lightbox="'.$val->id.'"><img class="example-image" src="'.$photoUrl.'" alt="thumb-1" width="50" height="50"></a>
                ');

        }


        $params['totalFound'] = count($totalObj);
        $totalPageCount =  ceil(count($totalObj) / $limit);

        $model=new EntryForm;
        $columnsArray = array('SIRA','KATEGORİ','ÜRÜN KODU','BAŞLIK','AÇIKLAMA','ALIŞ', 'SATIŞ 1','SATIŞ 2','ADET','BEDEN','RENK','FOTO');
        $this->render('excel',array('params' => $params,'model'=>$model,'rowsArray'=>$rowsArray,'columnsArray'=>$columnsArray,'totalCount'=>count($totalObj),'totalPageCount'=>$totalPageCount,'currentPage'=>$currentPage));
    }

    public function actionhadleExcelAjax() {
        $i = 0;
        while (isset($_POST['row-'.$i])) {
            $rowData = $_POST['row-'.$i];
            if (empty($rowData['FİYAT'])) {
                $rowData['FİYAT'] = null;
            } else {
                $rowData['FİYAT'] = str_replace(",",".",$rowData['FİYAT']);
            }
            $model = EntryForm::model()->findByPk($rowData['SIRA']);
            $model->attributes = $rowData;
            $model->code = $rowData['ÜRÜN KODU'];
            $model->title = $rowData['BAŞLIK'];
            $model->description = $rowData['AÇIKLAMA'];
            $model->quantity = $rowData['ADET'];
            $model->size = $rowData['BEDEN'];
            $model->color = $rowData['RENK'];
            $model->buy_price = $rowData['ALIŞ'];
            $model->sale_price1 = $rowData['SATIŞ 1'];
            $model->sale_price2 = $rowData['SATIŞ 2'];



            if ($model->save()) {

            }
            $i++;
        }
    }

//@resim_yukle($filePathName,$uploadThumbDir.$fileName,148,148,100);
    function resim_yukle($kaynak,$hedef,$h,$w,$kalite=90,$filigran=0,$temsili=0,$getimagesize=0,$fbgcolor='white') {
        global $config;
        @chmod($kaynak,0775);
        if ($getimagesize==0) {
            $image_info = getimagesize($kaynak);
        }
        else {
            $image_info = $getimagesize;
        }

        if($image_info['mime'] == 'image/jpg' || $image_info['mime']=='image/jpeg'|| $image_info['mime']=='image/pjpeg') {
            $srcimg=imagecreatefromjpeg($kaynak);
        }
        elseif($image_info['mime'] == 'image/png'|| $image_info['mime'] == 'image/x-png') {
            $srcimg=imagecreatefrompng($kaynak);
        }
        elseif($image_info['mime'] == 'image/gif') {
            $srcimg=imagecreatefromgif($kaynak);
        }

        # orjinal wd ,hi
        $o_wd = imagesx($srcimg) ;
        $o_ht = imagesy($srcimg) ;

        # hedef resim
        $t_im = imagecreatetruecolor($w,$h);

        if($fbgcolor=='white') {
            $fon=imagecolorallocate($t_im, 255, 255, 255);
        }
        elseif($fbgcolor=='gri') {
            $fon= imagecolorallocate($t_im, 156, 153, 151);
        }
        elseif($fbgcolor=='acikmavi') {
            $fon = imagecolorallocate($t_im, 224, 237, 246);
        }
        elseif($fbgcolor=='1D1D1D') {
            # siyah
            $fon = imagecolorallocate($t_im, 29, 29, 29);
        }
        else {
            # white
            $fon=imagecolorallocate($t_im, 255, 255, 255);
        }

        imagefilledrectangle($t_im, 0, 0, $w, $h, $fon);

        if($o_wd>$o_ht) {
            # resim yatik
            $new_width = $w;
            $new_height = ($o_ht*$new_width)/$o_wd;
        }
        elseif($o_wd<$o_ht || $o_wd==$o_ht) {
            # resim dik
            $new_height = $h;
            $new_width = ($o_wd*$new_height)/$o_ht;
        }

        # eger orjinal resim hedef boyuttan kucuk ise buyutunce bozuluyor, buyutme!
        if( ($o_wd<$w && $o_ht<$h)) {
            imagecopyresampled($t_im, $srcimg, ($w-$o_wd)/2, ($h-$o_ht)/2, 0, 0,$o_wd,$o_ht, $o_wd, $o_ht);
        }
        else {
            imagecopyresampled($t_im, $srcimg, ($w-$new_width)/2, ($h-$new_height)/2, 0, 0,$new_width,$new_height, $o_wd, $o_ht);
        }

        # üzerine filigran yazılacak mı?
        if($filigran==1) {
            $logo_file=$config['document_root']."/images/filigran.png";
            $img_logo = imagecreatefrompng($logo_file);
            $sx = imagesx($img_logo);
            $sy = imagesy($img_logo);
            imagecopy($t_im, $img_logo, round(($w-$sx)/2), round(($h-$sy)/2), 0, 0, $sx, $sy);
        }

        # üzerine "Temsili Fotoğraf" yazılacak mı?
        if($temsili==1) {
            $yazi=" Temsili Fotoğraf ";
            //$font=2;
            $font_file=$config['document_root']."/lib/monofont.ttf";
            $yer_x=110;
            $yer_y=15;

            $bg = imagecolorallocate($t_im, 255, 255, 255);
            $textcolor = imagecolorallocate($t_im, 0, 0, 0);
            ImageFilledRectangle($t_im, $w-$yer_x, $h-$yer_y-10, ($w-$yer_x)+88, ($h-$yer_y)+1, $bg);
            imagettftext($t_im, 9, 0, $w-$yer_x, $h-$yer_y, $textcolor, $font_file, $yazi);
            //imagestring($t_im, $font, $w-$yer_x, $h-$yer_y, $yazi, $textcolor);
        }

        if($image_info['mime'] == 'image/jpg' || $image_info['mime']=='image/jpeg'|| $image_info['mime']=='image/pjpeg') {
            imagejpeg($t_im,$hedef,$kalite);
        }
        elseif($image_info['mime'] == 'image/png'|| $image_info['mime'] == 'image/x-png') {
            imagepng($t_im,$hedef);
        }
        elseif($image_info['mime'] == 'image/gif') {
            imagegif($t_im,$hedef);
        }
        imagedestroy($t_im);
    }
}